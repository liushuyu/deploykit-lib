use std::fs::File;
use std::io::{self, Seek, Write};
use std::mem::transmute;
use std::path::Path;

use nix::sys::reboot::{reboot, RebootMode};
use nix::sys::signal::{kill, Signal};
use nix::unistd::{sync, sysconf, Pid, SysconfVar};
use nix::{ioctl_read, ioctl_write_int};

const FS_NOCOW_FL: libc::c_ulong = 0x00800000; /* Do not cow file */

/// swap version
const SWAP_VERSION: u32 = 1;
/// swap magic numbers
const SWAP_SIGNATURE: &[u8; 10usize] = b"SWAPSPACE2";
/// enable discard for swap
const SWAP_FLAG_DISCARD: u32 = 0x10000;
/// discard swap area at swapon-time
const SWAP_FLAG_DISCARD_ONCE: u32 = 0x20000;
/// discard page-clusters after use
const SWAP_FLAG_DISCARD_PAGES: u32 = 0x40000;
/// maxium page size supported by the kernel
const MAX_PAGE_SIZE: usize = 64 * 1024;

// IOCTL definitions for filesystem operations.
// See `/usr/include/ext2fs/ext2_fs.h` for more information.

ioctl_read! {
    /// Get file attribute flags from the filesystem.
    /// C constant: `EXT2_IOC_GETFLAGS`
    fgetflags, b'f', 1, libc::c_ulong
}

ioctl_write_int! {
    /// Set file attribute flags to the specified file on the filesystem.
    /// C constant: `EXT2_IOC_SETFLAGS`
    fsetflags, b'f', 2
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
struct SwapHeaderV1_2 {
    pub bootbits: [libc::c_char; 1024usize],
    pub version: u32,
    pub last_page: u32,
    pub nr_badpages: u32,
    pub uuid: [libc::c_uchar; 16usize],
    pub volume_name: [libc::c_char; 16usize],
    pub padding: [u32; 117usize],
    pub badpages: [u32; 1usize],
}

impl SwapHeaderV1_2 {
    pub fn new(uuid: &uuid::Uuid, num_pages: u32) -> SwapHeaderV1_2 {
        Self {
            bootbits: [0; 1024usize],
            version: SWAP_VERSION,
            last_page: num_pages - 1,
            nr_badpages: 0,
            uuid: uuid.to_owned().into_bytes(),
            volume_name: [0; 16usize],
            padding: [0; 117usize],
            badpages: [0; 1usize],
        }
    }
}

pub struct OsSettings<'a> {
    prefix: &'a Path,
    hostname: &'a str,
    locale: &'a str,
    rtc_is_utc: bool,
}

impl<'a> OsSettings<'a> {
    pub fn new(
        prefix: &'a Path,
        hostname: &'a str,
        locale: &'a str,
        rtc_is_utc: bool,
    ) -> OsSettings<'a> {
        Self {
            prefix,
            hostname,
            locale,
            rtc_is_utc,
        }
    }

    /// Setup all the necessary settings for the OS
    pub fn setup(&self) -> io::Result<()> {
        set_hostname(self.prefix, self.hostname)?;
        set_locale(self.prefix, self.locale)?;
        set_rtc_time_type(self.prefix, self.rtc_is_utc)?;

        Ok(())
    }
}

/// Sets hostname in the guest environment
fn set_hostname(prefix: &Path, name: &str) -> io::Result<()> {
    let mut f = File::create(prefix.join("etc/hostname"))?;

    Ok(f.write_all(name.as_bytes())?)
}

/// Sets locale in the guest environment
fn set_locale(prefix: &Path, locale: &str) -> io::Result<()> {
    let mut f = File::create(prefix.join("etc/locale.conf"))?;
    f.write_all(b"LANG=")?;

    Ok(f.write_all(locale.as_bytes())?)
}

/// Sets RTC timezone in the guest environment (UTC or Local)
fn set_rtc_time_type(prefix: &Path, utc: bool) -> io::Result<()> {
    let mut f = File::create(prefix.join("etc/adjtime"))?;
    f.write_all(b"0.000000 0 0.000000\n0\n")?;

    Ok(f.write_all(if utc { b"UTC\n" } else { b"Local\n" })?)
}

/// Safely reboot the system after disk data has been synchronized
pub fn sync_and_reboot() -> io::Result<()> {
    sync();
    kill(Pid::from_raw(1), Signal::SIGINT)?;

    Ok(())
}

/// Add additional flags to the given file
fn append_file_flags(fd: std::os::unix::prelude::RawFd, new_flags: libc::c_ulong) -> io::Result<libc::c_ulong> {
    let mut flags: libc::c_ulong = 0;
    unsafe { fgetflags(fd, &mut flags)? };
    flags |= new_flags;
    unsafe { fsetflags(fd, flags)? };

    Ok(flags)
}

/// Reboot the system IMMEDIATELY (no `sync` will happen)
/// UNSAFE: This function is unsafe because this reboot process may corrupt disk data
pub unsafe fn panic_reboot() -> nix::Result<core::convert::Infallible> {
    reboot(RebootMode::RB_AUTOBOOT)
}

/// Create a new swapfile with the specified path and size
pub fn create_swapfile(path: &Path, size: u64) -> io::Result<()> {
    use std::os::unix::prelude::AsRawFd;
    use nix::sys::stat::{fchmod, Mode};

    let new_uuid = uuid::Uuid::new_v4();
    let system_pagesize = sysconf(SysconfVar::PAGE_SIZE)?
        .ok_or_else(|| io::Error::new(io::ErrorKind::Unsupported, "kernel page size unknown"))?;
    if system_pagesize < 0 || system_pagesize > MAX_PAGE_SIZE as i64 {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "impossible kernel page size",
        ));
    }
    let num_pages = size / (system_pagesize as u64);
    let actual_num_pages = if num_pages > u32::MAX.into() {
        // swap size should not exceed 17.59 TiB
        u32::MAX
    } else {
        num_pages as u32
    };
    // write swapfile header
    let header = SwapHeaderV1_2::new(&new_uuid, actual_num_pages);
    let actual_size = actual_num_pages as u64 * system_pagesize as u64;
    let buffer = [0u8; 4096];
    let mut bytes_remaining = actual_size;
    let mut f = File::create(path)?;
    // TODO: check errors
    append_file_flags(f.as_raw_fd(), FS_NOCOW_FL).ok();
    while bytes_remaining > 0 {
        if bytes_remaining > 4096 {
            bytes_remaining -= f.write(&buffer)? as u64;
        } else {
            bytes_remaining -= f.write(&buffer[..bytes_remaining as usize])? as u64;
        }
    }
    f.seek(io::SeekFrom::Start(0))?;
    let header_data: [u8; std::mem::size_of::<SwapHeaderV1_2>()] = unsafe { transmute(header) };
    f.write_all(&header_data)?;
    let sig_offset = system_pagesize as usize - SWAP_SIGNATURE.len();
    f.seek(io::SeekFrom::Start(sig_offset as u64))?;
    f.write_all(SWAP_SIGNATURE)?;
    // chmod 0000
    fchmod(f.as_raw_fd(), Mode::empty())?;
    f.sync_all()?;

    Ok(())
}

#[test]
fn bindgen_test_layout_swap_header_v1_2() {
    assert_eq!(
        ::std::mem::size_of::<SwapHeaderV1_2>(),
        1540usize,
        concat!("Size of: ", stringify!(SwapHeaderV1_2))
    );
    assert_eq!(
        ::std::mem::align_of::<SwapHeaderV1_2>(),
        4usize,
        concat!("Alignment of ", stringify!(SwapHeaderV1_2))
    );
}
