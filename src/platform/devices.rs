use std::io::BufRead;

use anyhow::Result;
use nvidia_gpu_data::*;
use udev::Enumerator;

const BCM_VENDOR_ID_STRING: &str = "0x14e4";
const BCM_FW_PATTERNS: &[&str] = &["b43/ucode", "b43legacy/ucode"];

fn list_all_partitions() -> Result<()> {
    let mut enumerator = Enumerator::new()?;
    enumerator.match_subsystem("block")?;
    enumerator.match_property("DEVTYPE", "partition")?;
    for device in enumerator.scan_devices()? {
        println!("\nDevice: {:?}", device.devpath());
        for p in device.properties() {
            println!("{:?} = {:?}", p.name(), p.value());
        }
        // dbg!(device);
    }

    Ok(())
}

fn parse_numeric_id(s: &std::ffi::OsStr) -> u16 {
    u16::from_str_radix(s.to_string_lossy().trim_start_matches("0x"), 16).unwrap_or_default()
}

pub fn detect_nvidia_gpus() -> Result<Vec<&'static CardInfo>> {
    let mut enumerator = Enumerator::new()?;
    enumerator.match_attribute("vendor", NVIDIA_VENDOR_ID_STRING)?;
    let mut result = Vec::with_capacity(3);

    for device in enumerator.scan_devices()? {
        let device_id = device.attribute_value("device").unwrap_or_default();
        let id = parse_numeric_id(device_id);
        let sub_id = parse_numeric_id(
            device
                .attribute_value("subsystem_device")
                .unwrap_or_default(),
        );
        let sub_vendor = parse_numeric_id(
            device
                .attribute_value("subsystem_vendor")
                .unwrap_or_default(),
        );
        let data = NVIDIA_GPU_DATA.iter().find(|x| {
            x.devid == id
                && match x.subdevid {
                    Some(v) => v == sub_id,
                    None => true,
                }
                && match x.subvendorid {
                    Some(d) => d == sub_vendor,
                    None => true,
                }
        });
        if let Some(data) = data {
            result.push(data);
        }
    }

    Ok(result)
}

pub fn detect_b43_wireless() -> Result<bool> {
    use aho_corasick::AhoCorasickBuilder;
    use std::io::BufReader;
    use std::os::unix::prelude::OpenOptionsExt;

    let mut enumerator = Enumerator::new()?;
    enumerator.match_attribute("vendor", BCM_VENDOR_ID_STRING)?;
    let bcm_present = enumerator.scan_devices()?.next().is_some();

    if !bcm_present {
        let f = std::fs::File::options()
            .read(true)
            .custom_flags(libc::O_NONBLOCK)
            .open("/dev/kmsg")?;
        let matcher = AhoCorasickBuilder::default()
            .auto_configure(BCM_FW_PATTERNS)
            .build(BCM_FW_PATTERNS);
        let reader = BufReader::new(f);
        // TODO: blocking
        for line in reader.split(b'\n') {
            if let Ok(line) = line {
                if matcher.earliest_find(&line).is_some() {
                    return Ok(true);
                }
            }
        }
    }

    Ok(false)
}

#[test]
fn test_device_listing() {
    dbg!(detect_b43_wireless());
}
