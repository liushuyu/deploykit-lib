use std::ffi::{CStr, CString};
use std::fmt::Write as WriteFmt;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::Path;

use anyhow::{anyhow, Result};
use libc::c_char;
use nom::bytes::complete::{take_until, take_until1};
use nom::character::complete::char;
use nom::combinator::map;
use nom::multi::many1;
use nom::sequence::{preceded, terminated};
use nom::{IResult, ParseTo};
use rand::{distributions::Slice, Rng, SeedableRng};
use rand_chacha::ChaChaRng;

const CRYPT_DATA_SIZE: usize = 32768; // only applicapable to glibc

const MIN_USER_UID: u32 = 1000;
const MIN_USER_GID: u32 = 1000;

const SALT_LOOKUP: &[u8] = &[
    0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44,
    0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54,
    0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a,
    0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a,
];

#[derive(Clone, Copy)]
enum HiveType {
    Passwd,
    Group,
}

/// UserId(uid, gid)
#[derive(Clone, Copy)]
struct UserId(u32, u32);

impl std::fmt::Display for HiveType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                HiveType::Passwd => "passwd",
                HiveType::Group => "group",
            }
        )
    }
}

#[link(name = "crypt")]
extern "C" {
    /// Reference: https://man7.org/linux/man-pages/man3/crypt.3.html
    fn crypt_r(phrase: *const c_char, salt: *const u8, data: *mut u8) -> *mut c_char;
}

fn encrypt_password(password: &str) -> Result<String> {
    let mut buffer = [0u8; CRYPT_DATA_SIZE];
    let rng = ChaChaRng::from_entropy();
    let salt_value = rng
        .sample_iter(Slice::new(&SALT_LOOKUP).unwrap())
        .take(16)
        .collect::<Vec<_>>();
    let mut salt = Vec::from(&b"$6$"[..]);
    salt.extend(salt_value);
    let password = CString::new(password)?;
    let result = unsafe { crypt_r(password.as_ptr(), salt.as_ptr(), buffer.as_mut_ptr()) };

    if result.is_null() {
        Err(anyhow!("Password hashing failed."))
    } else {
        let encrypted = unsafe { CStr::from_ptr(result) }
            .to_str()
            .and_then(|s| Ok(s.to_string()))?;
        Ok(encrypted)
    }
}

fn create_new_shadow_entry(user: &str, encrypted_password: &str) -> Result<String> {
    use std::time::{SystemTime, UNIX_EPOCH};

    let entry_len = user.len() + encrypted_password.len() + 22;
    let mut entry = String::with_capacity(entry_len);
    let pw_age = SystemTime::now().duration_since(UNIX_EPOCH)?.as_secs() / 86400;
    writeln!(
        &mut entry,
        "{}:{}:{}:0:99999:7:::",
        user, encrypted_password, pw_age
    )?;

    Ok(entry)
}

#[inline]
fn take_and_eat_colon(s: &[u8]) -> IResult<&[u8], &[u8]> {
    terminated(take_until1(":"), char(':'))(s)
}

#[inline]
fn take_column_3(s: &[u8]) -> IResult<&[u8], &[u8]> {
    preceded(
        take_and_eat_colon,
        preceded(take_and_eat_colon, take_and_eat_colon),
    )(s)
}

#[inline]
fn skip_to_next_line(s: &[u8]) -> IResult<&[u8], &[u8]> {
    take_until("\n")(s)
}

#[inline]
fn parse_oneline(s: &[u8]) -> IResult<&[u8], &[u8]> {
    terminated(take_column_3, skip_to_next_line)(s)
}

#[inline]
fn collect_all_ids(s: &[u8]) -> IResult<&[u8], Vec<u32>> {
    many1(map(parse_oneline, |v| {
        let id: u32 = v.parse_to().unwrap_or(0);
        id
    }))(s)
}

fn get_next_numeric_id(hive_content: &[u8], hive_type: &HiveType) -> Result<u32> {
    let ids =
        collect_all_ids(hive_content).map_err(|_| anyhow!("Unable to parse {} file", hive_type))?;
    let max_id = ids.1.iter().max().unwrap_or(&MIN_USER_UID);

    if max_id < &MIN_USER_UID {
        Ok(MIN_USER_UID)
    } else {
        Ok(*max_id + 1)
    }
}

fn write_new_group_entry(prefix: &Path, user: &str) -> Result<u32> {
    let file = prefix.join("etc/group");
    let mut f = File::options().append(true).read(true).open(&file)?;
    let mut buffer = Vec::with_capacity(2048);
    f.read_to_end(&mut buffer)?;

    let next_gid = get_next_numeric_id(&buffer, &HiveType::Group).unwrap_or(MIN_USER_GID);
    writeln!(f, "{}:x:{}:", user, next_gid)?;

    Ok(next_gid)
}

fn write_new_user_entry(prefix: &Path, user: &str, fullname: &str) -> Result<UserId> {
    let file = prefix.join("etc/passwd");
    let mut f = File::options().append(true).read(true).open(&file)?;
    let mut buffer = Vec::with_capacity(4096);
    f.read_to_end(&mut buffer)?;
    // check if user exists
    let check_string = format!("{}:x:", user);
    if memchr::memmem::find(&buffer, check_string.as_bytes()).is_some() {
        return Err(anyhow!("user-already-exists"));
    }

    let next_uid = get_next_numeric_id(&buffer, &HiveType::Passwd).unwrap_or(MIN_USER_UID);
    let next_gid = write_new_group_entry(prefix, user)?;
    writeln!(
        f,
        "{}:x:{}:{}:{}:/home/{}:/bin/bash",
        user, next_uid, next_gid, fullname, user
    )?;

    Ok(UserId(next_uid, next_gid))
}

fn create_user_home(prefix: &Path, user: &str, user_id: &UserId) -> Result<()> {
    use nix::fcntl::readlink;
    use nix::sys::stat::{fchmodat, FchmodatFlags, Mode};
    use nix::unistd::{chown, Gid, Uid};
    use std::fs::DirBuilder;
    use std::os::unix::fs::{DirBuilderExt, MetadataExt};
    use walkdir::WalkDir;

    let owner = Some(Uid::from_raw(user_id.0));
    let group = Some(Gid::from_raw(user_id.1));
    let chown_to_target_user =
        |target: &Path| -> Result<(), nix::Error> { chown(target, owner, group) };

    let target = prefix.join(format!("home/{}", user));
    let mut builder = DirBuilder::new();
    builder.mode(0o700).recursive(true).create(&target)?;
    chown_to_target_user(&target)?;
    // copy skeleton files, if any
    let skel_dir = prefix.join("etc/skel");
    let walker = WalkDir::new(&skel_dir).min_depth(1);
    for entity in walker.into_iter() {
        let entity = entity?;
        let file_type = entity.file_type();
        let target_path = match entity.path().strip_prefix(&skel_dir) {
            Ok(v) => target.join(v),
            Err(_) => continue,
        };
        let metadata = entity.metadata()?;
        let perm = Mode::from_bits_truncate(metadata.mode() & 0o777);
        if file_type.is_file() {
            std::fs::copy(entity.path(), &target_path)?;
        } else if file_type.is_dir() {
            std::fs::create_dir(&target_path)?;
        } else if file_type.is_symlink() {
            let sym_target = readlink(entity.path())?;
            std::os::unix::fs::symlink(sym_target, &target_path)?;
        } else {
            continue; // ignore other file types
        }
        fchmodat(None, &target_path, perm, FchmodatFlags::NoFollowSymlink)?;
        chown_to_target_user(&target_path)?;
    }

    Ok(())
}

/// Create a new user with their home directory in the specified prefix (alternative root)
pub fn create_new_user(prefix: &Path, user: &str, fullname: &str, password: &str) -> Result<()> {
    let password = encrypt_password(password)?;
    let shadow_entry = create_new_shadow_entry(user, &password)?;
    let shadow_path = prefix.join("etc/shadow");
    let mut shadow_file = File::options().append(true).open(&shadow_path)?;
    shadow_file.write_all(shadow_entry.as_bytes())?;

    let user_id = write_new_user_entry(prefix, user, fullname)?;
    create_user_home(prefix, user, &user_id)?;

    Ok(())
}

#[test]
fn test_password_hash() {
    let hash = encrypt_password("test").unwrap();
    assert_eq!(hash.len(), 106);
    assert!(hash.starts_with("$6$"));
}

#[test]
fn test_shadow_entry() {
    let entry = create_new_shadow_entry("name", "$6$password").unwrap();
    assert_eq!(entry.len(), 36);
}

#[test]
fn fullname_normalization() {
    use deunicode::deunicode;

    assert_eq!(deunicode("白铭骢"), "Bai Ming Cong");
    assert_eq!(deunicode("げんまい🍵"), "genmaitea");
}

#[test]
fn test_extract_column_3() {
    assert_eq!(take_column_3(&b"1:2:3:4"[..]), Ok((&b"4"[..], &b"3"[..])));
    assert_eq!(take_column_3(&b"1:2:30:4"[..]), Ok((&b"4"[..], &b"30"[..])));
}

#[test]
fn test_collect_ids() {
    assert_eq!(
        collect_all_ids(
            &b"root:x:0:brltty,root\nbin:x:1:root,bin,daemon\ndaemon:x:2:root,bin,daemon\n"[..]
        ),
        Ok((&b"\n"[..], vec![0, 1, 2]))
    );
    static TEST_DATA: &str = r#"avahi:x:84:
sddm:x:996:
polkitd:x:102:
colord:x:124:
rtkit:x:133:
mysql:x:89:
"#;
    assert_eq!(
        collect_all_ids(TEST_DATA.as_bytes()),
        Ok((&b"\n"[..], vec![84, 996, 102, 124, 133, 89]))
    );
}

#[test]
fn test_get_next_id() {
    static TEST_DATA: &str = r#"avahi:x:84:
sddm:x:996:
polkitd:x:102:
colord:x:124:
rtkit:x:133:
mysql:x:89:
"#;
    assert_eq!(
        get_next_numeric_id(TEST_DATA.as_bytes(), &HiveType::Group).unwrap(),
        1000
    );
    static TEST_DATA_2: &str = r#"avahi:x:84:
sddm:x:996:
polkitd:x:102:
normaluser2:x:1002:
colord:x:124:
rtkit:x:133:
mysql:x:89:
normaluser:x:1000:
"#;
    assert_eq!(
        get_next_numeric_id(TEST_DATA_2.as_bytes(), &HiveType::Group).unwrap(),
        1003
    );
    static TEST_DATA_3: &str = r#"systemd-timesync:x:192:192:systemd-timesync:/:/usr/bin/nologin
systemd-network:x:193:193:systemd-network:/:/usr/bin/nologin
systemd-bus-proxy:x:194:194:systemd-bus-proxy:/:/usr/bin/nologin
systemd-resolve:x:195:195:systemd-resolve:/:/usr/bin/nologin
systemd-coredump:x:997:997:systemd Core Dumper:/:/sbin/nologin
systemd-journal-remote:x:998:998:systemd Journal Remote:/:/sbin/nologin
systemd-journal-upload:x:999:999:systemd Journal Upload:/:/sbin/nologin
"#;
    assert_eq!(
        get_next_numeric_id(TEST_DATA_3.as_bytes(), &HiveType::Passwd).unwrap(),
        1000
    );
    static TEST_DATA_4: &str = r#"systemd-timesync:x:192:192:systemd-timesync:/:/usr/bin/nologin
systemd-network:x:193:193:systemd-network:/:/usr/bin/nologin
systemd-bus-proxy:x:194:194:systemd-bus-proxy:/:/usr/bin/nologin
systemd-resolve:x:195:195:systemd-resolve:/:/usr/bin/nologin
normaluser2:x:1002:1002:Normal User🍵:/home/normaluser2:/bin/bash
systemd-coredump:x:997:997:systemd Core Dumper:/:/sbin/nologin
normaluser:x:1000:1000:Normal User:/home/normaluser:/bin/bash
systemd-journal-remote:x:998:998:systemd Journal Remote:/:/sbin/nologin
systemd-journal-upload:x:999:999:systemd Journal Upload:/:/sbin/nologin
"#;
    assert_eq!(
        get_next_numeric_id(TEST_DATA_4.as_bytes(), &HiveType::Passwd).unwrap(),
        1003
    );
}
