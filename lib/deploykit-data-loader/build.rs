use std::fs::File;
use std::io::{Write, BufReader, BufRead};

include!("src/parsers.rs");

fn generate_reserved_userlist() {
    let original_file = File::open("data/reservedusers").unwrap();
    let mut output = File::create(format!(
        "{}/reservedusers.gen.rs",
        std::env::var("OUT_DIR").unwrap()
    ))
    .unwrap();
    writeln!(
        &mut output,
        "const RESERVED_USER_LIST: &'static [&'static str] = &["
    )
    .unwrap();
    for line in BufReader::new(original_file).lines() {
        let line = line.unwrap();
        let trimmed = line.trim();
        if trimmed.is_empty() { continue; }
        writeln!(
            &mut output,
            "\"{}\",",
            trimmed
        ).unwrap();
    }
    writeln!(&mut output, "];").unwrap();
}

fn generate_langlist() {
    let original_file = File::open("data/languagelist").unwrap();
    let mut output = File::create(format!(
        "{}/languagelist.gen.rs",
        std::env::var("OUT_DIR").unwrap()
    ))
    .unwrap();
    writeln!(
        &mut output,
        "const LANGUAGE_LIST: &'static [LanguageData] = &["
    )
    .unwrap();
    let list = parse_langlist(original_file).expect("unable to parse languagelist file!");
    for lang in list {
        writeln!(
            &mut output,
            "LanguageData {{ name_english: \"{}\", name_native: \"{}\", locale: \"{}\" }},",
            lang.name_english, lang.name_native, lang.locale
        )
        .unwrap();
    }
    writeln!(&mut output, "];").unwrap();
}

fn main() {
    generate_langlist();
    generate_reserved_userlist();
}
