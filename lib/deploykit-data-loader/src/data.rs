//! Data loaders for generated or system data

use std::time::SystemTime;

use crate::LanguageData;

include!(concat!(env!("OUT_DIR"), "/languagelist.gen.rs"));
include!(concat!(env!("OUT_DIR"), "/reservedusers.gen.rs"));

#[derive(Debug, Clone)]
pub struct TimezoneData {
    pub name: &'static str,
    pub raw_offset: i32,
    pub offset_str: String,
}

/// Load languages list
#[inline]
pub fn load_languagelist() -> &'static [LanguageData] {
    LANGUAGE_LIST
}

/// Load the reserved users list
#[inline]
pub fn load_reservedusers() -> &'static [&'static str] {
    RESERVED_USER_LIST
}

#[inline]
fn format_offset(offset: i32) -> String {
    let mut result = String::with_capacity(10);
    if offset < 0 {
        result.push('-');
    } else {
        result.push('+');
    }
    let offset = offset.abs();
    let hour = offset / 3600;
    let minute = offset % 3600 / 60;
    result.push_str(&format!("{:02}:{:02}", hour, minute));

    result
}

/// Get the version number of the tzdata in use by this application
pub fn get_tzdata_version() -> &'static str {
    tzdb::VERSION
}

/// Load the time zone data for the current calendar day
pub fn load_tzdata_temporal() -> Result<Vec<TimezoneData>, Box<dyn std::error::Error>> {
    let timestamp = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)?
        .as_secs();
    let mut result = Vec::with_capacity(tzdb::TZ_NAMES.len());
    for tz in tzdb::TZ_NAMES {
        let this_tz = tzdb::tz_by_name(tz).unwrap();
        if let Ok(time_type) = this_tz.find_local_time_type(timestamp as i64) {
            let raw_offset = time_type.ut_offset();
            result.push(TimezoneData {
                name: tz,
                raw_offset,
                offset_str: format_offset(raw_offset),
            });
        }
    }

    Ok(result)
}
