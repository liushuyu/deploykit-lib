use std::{error::Error, io::Read};

#[derive(Debug, Clone, PartialEq)]
pub struct LanguageInfo {
    name_english: String, // English name
    name_native: String,  // Native name
    locale: String,       // Language code
}

#[derive(Debug, Clone, Copy)]
pub struct LanguageData {
    pub name_english: &'static str, // English name
    pub name_native: &'static str,  // Native name
    pub locale: &'static str,       // Language code
}

/// Parse Ubuntu's language file (from Ubiquitous installer)
pub fn parse_langlist<R: Read>(input: R) -> Result<Vec<LanguageInfo>, Box<dyn Error>> {
    let reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b';')
        .comment(Some(b'#'))
        .from_reader(input);
    let mut languages = Vec::with_capacity(120);

    for result in reader.into_records() {
        let record = result?;
        languages.push(LanguageInfo {
            name_english: record[1].to_owned(),
            name_native: record[2].to_owned(),
            locale: record[5].to_owned(),
        });
    }

    Ok(languages)
}

#[test]
fn test_parse_langlist() {
    let test_data = "bg;Bulgarian;Български;2;BG;bg_BG.UTF-8;;console-setup\nzh_CN;Chinese (Simplified);中文(简体);3;CN;zh_CN.UTF-8;zh_CN:zh;\n";
    assert_eq!(
        parse_langlist(test_data.as_bytes()).unwrap(),
        vec![
            LanguageInfo {
                name_english: "Bulgarian".to_string(),
                name_native: "Български".to_string(),
                locale: "bg_BG.UTF-8".to_string()
            },
            LanguageInfo {
                name_english: "Chinese (Simplified)".to_string(),
                name_native: "中文(简体)".to_string(),
                locale: "zh_CN.UTF-8".to_string()
            }
        ]
    );
}
