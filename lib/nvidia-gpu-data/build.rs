use serde::Deserialize;
use std::io::Write;

#[derive(Deserialize, Debug, Clone)]
struct CardInfoBuild {
    devid: String,
    subdevid: Option<String>,
    subvendorid: Option<String>,
    legacybranch: Option<String>,
    name: String,
    features: Vec<String>,
}

#[derive(Deserialize)]
struct GPUData {
    chips: Vec<CardInfoBuild>,
}

fn main() {
    let f =
        std::fs::File::open("./supported-gpus.json").expect("Failed to open supported-gpus.json");
    let data: GPUData = serde_json::from_reader(&f).unwrap();
    let mut output =
        std::fs::File::create(format!("{}/gpudata.rs", std::env::var("OUT_DIR").unwrap())).unwrap();
    writeln!(output, "/// NVIDIA GPU data generated from supported-gpus.json\npub const NVIDIA_GPU_DATA: &'static [CardInfo] = &[").unwrap();
    for chip in data.chips {
        writeln!(
            output,
            "CardInfo {{ is_legacy: {}, devid: {}, subdevid: {}, subvendorid: {}, name: \"{}\"{} }},",
            if chip.legacybranch.is_some() {
                "true"
            } else {
                "false"
            },
            chip.devid,
            match chip.subdevid {
                Some(v) => format!("Some({})", v),
                None => "None".to_string(),
            },
            match chip.subvendorid {
                Some(v) => format!("Some({})", v),
                None => "None".to_string(),
            },
            chip.name,
            if cfg!(feature = "gpu-features") {
                serde_json::to_string(&chip.features).unwrap()
            } else {
                String::new()
            }
        )
        .unwrap();
    }
    writeln!(output, "];").unwrap();
}
