#!/bin/sh -e

BASE_URL='https://download.nvidia.com/XFree86/Linux-x86_64/'

LATEST_VERSION="$(curl "${BASE_URL}latest.txt" | cut -d' ' -f1)"
echo "Latest version: $LATEST_VERSION"

TMPDIR="$(mktemp -d)"
TMPFILE="$(mktemp)"
TOPDIR="$(dirname "$0")"
cd "${TMPDIR}"
curl -o "${TMPFILE}" -L "${BASE_URL}${LATEST_VERSION}/NVIDIA-Linux-x86_64-${LATEST_VERSION}-no-compat32.run"
chmod a+x "${TMPFILE}"
"${TMPFILE}" -x
cp -v "NVIDIA-Linux-x86_64-${LATEST_VERSION}-no-compat32"/supported-gpus/supported-gpus.json "$TOPDIR"/
cp -v "NVIDIA-Linux-x86_64-${LATEST_VERSION}-no-compat32"/supported-gpus/LICENSE "$TOPDIR"/LICENSE-data

echo "Done."
cd "$TOPDIR"
rm -rf "${TMPFILE}" "${TMPDIR}"
