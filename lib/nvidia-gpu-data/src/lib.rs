#[derive(Clone, Copy, Debug)]
pub struct CardInfo {
    pub devid: u16,
    pub subdevid: Option<u16>,
    pub subvendorid: Option<u16>,
    pub name: &'static str,
    pub is_legacy: bool,
    #[cfg(feature = "gpu-features")]
    pub features: &'static [&'static str],
}

include!(concat!(env!("OUT_DIR"), "/gpudata.rs"));

pub const NVIDIA_VENDOR_ID: u16 = 0x10DE;
pub const NVIDIA_VENDOR_ID_STRING: &str = "0x10de";
